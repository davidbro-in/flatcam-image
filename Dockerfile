FROM ubuntu:18.04

LABEL maintainer davidmbroin@gmail.com

RUN apt update &&\
    DEBIAN_FRONTEND=noninteractive apt install -y \
        software-properties-common \
        python \
        libx11-6 \
        libfreetype6 \
        libxrender1 \
        libfontconfig1 \
        libxext6 \
        xvfb \
        psmisc\
        zip \    
        git  &&\
    rm -rf /var/lib/apt/list/* 

RUN git clone https://bitbucket.org/jpcgt/flatcam &&\
        cd flatcam &&\
        git checkout 5f2b63442ffb4eb664bc53c639c92f8521cd118f 

COPY setup_ubuntu.sh /flatcam/setup_ubuntu.sh
RUN  cd flatcam && sh setup_ubuntu.sh
